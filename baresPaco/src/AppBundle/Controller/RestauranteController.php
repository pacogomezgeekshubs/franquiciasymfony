<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use AppBundle\Form\RestauranteType;

use AppBundle\Entity\Restaurante;
use AppBundle\Entity\Plato;

/**
 * * @Route("/restaurantes")
 */
class RestauranteController extends Controller
{
    /**
     * @Route("/nuevo", name="nuevoRestaurante")
    */
    public function nuevoRestauranteAction(Request $request)
    {
        // creates a task and gives it some dummy data for this example
        $restaurante = new Restaurante();

        $form = $this->createForm(RestauranteType::class, $restaurante);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $restaurante = $form->getData();

    
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist( $restaurante);
            $entityManager->flush();
    
            dump($restaurante);

            return $this->redirectToRoute('actualizarRestaurante',['id'=>$restaurante->getId()]);
        }
        return $this->render('restaurante/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }
    /**
     * @Route("/actualizar/{id}", name="actualizarRestaurante")
    */
    public function actualizarRestauranteAction(Request $request,$id)
    {
        $repository = $this->getDoctrine()->getRepository(Restaurante::class);
        $restaurante = $repository->find($id);

        $form = $this->createFormBuilder($restaurante)
            ->add('nombre', TextType::class)
            ->add('localidad', TextType::class)
            ->add('Actualizar', SubmitType::class, ['label' => 'Actualizar Restaurante'])
            ->getForm();

        return $this->render('restaurante/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}