<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use AppBundle\Entity\Restaurante;

/**
     * @Route("/franquicias")
 */
class FranquiciasController extends Controller
{
    /**
     * @Route("/listado", name="franquicias")
     */
    public function indexAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository(Restaurante::class);

        $restaurantes = $repository->findAll();

        dump($restaurantes);
        return $this->render('franquicias/index.html.twig',['restaurantes'=>$restaurantes]);
    }
    

}
