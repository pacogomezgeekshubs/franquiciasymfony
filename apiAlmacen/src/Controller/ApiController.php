<?php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

use App\Entity\Producto;

    /**
     * @Route("/api/v1")
    */
class ApiController extends AbstractController
{
    /**
     * @Route("/almacenes", name="listarAlmacenes", methods={"GET"})
    */
    public function listarAlmacenesAction(Request $request)
    {
        $productos=
            [
             ['id'=>1,
            'nombre'=>'huevos'],
            ['id'=>2,
            'nombre'=>'fresas']
            ];
        $response =new JsonResponse($productos); 
        $response->headers->set('Access-Control-Allow-Origin', '*');
        return $response;
    }

    /**
     * @Route("/productos", name="listarProductos", methods={"GET"})
    */
    public function listarProductos(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository(Producto::class);
        $productos=$repository->findAll();
        foreach ($productos as $producto) {
            $productosArray[]=[
                "id"=>$producto->getId(),
                "nombre"=>$producto->getNombre()
            ];
        }
        $encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];

        $serializer = new Serializer($normalizers, $encoders);

        $jsonContent = $serializer->serialize($productos, 'json');
        $response = new Response($jsonContent);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }
}