<?php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController
{
    /**
     * @Route("/", name="homepage")
    */
    public function indexAction(Request $request)
    {

        return new Response(
            '<html><body>LHola mundo</body></html>'
        );
    }
}