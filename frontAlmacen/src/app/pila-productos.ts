import { Producto } from './producto';

export const PRODUCTOS: Producto[] = [
  { id: 11, nombre: 'Huevos' },
  { id: 12, nombre: 'Atun' },
  { id: 13, nombre: 'Ensalada' },
  { id: 14, nombre: 'Arroz' },
  { id: 15, nombre: 'Tomates' },
  { id: 16, nombre: 'Caldo' }
];