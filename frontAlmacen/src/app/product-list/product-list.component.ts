import { Component, OnInit } from '@angular/core';
import { ProductServiceService } from '../product-service.service';
import { Producto } from '../producto';
import { PRODUCTOS } from '../pila-productos';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {
  productos: Producto[];

  constructor(private productService: ProductServiceService) { }

  getProducts() {
    this.productService.getProducts().subscribe(data=> this.productos=data);
  } 

  ngOnInit() {
    this.getProducts();
  }

}
