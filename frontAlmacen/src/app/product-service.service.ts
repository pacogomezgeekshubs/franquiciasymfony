import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Producto } from './producto';
import { PRODUCTOS } from './pila-productos';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProductServiceService {

  private productosUrl = 'http://127.0.0.1:8000/api/v1/almacenes';

  constructor(private http: HttpClient) { }

  
  getProducts(): Observable<Producto[]> {
    return this.http.get<Producto[]> (this.productosUrl, {responseType: 'json'})
  }
}
